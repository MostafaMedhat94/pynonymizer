from datetime import date, datetime
from pynonymizer.database.exceptions import UnsupportedColumnStrategyError
from pynonymizer.strategy.update_column import UpdateColumnStrategyTypes
from pynonymizer.fake import FakeDataType
"""
All Static query generation functions
"""
_FAKE_COLUMN_TYPES = {
    FakeDataType.STRING: "TEXT",
    FakeDataType.DATE: "DATE",
    FakeDataType.DATETIME: "DATETIME",
    FakeDataType.INT: "INT"
}

# For preservation of unique values across versions of mysql, and this bug:
# https://bugs.mysql.com/bug.php?id=89474, use md5 based rand subqueries for unique values (rather than UUIDs)
_RAND_MD5 = "MD5(FLOOR((NOW() + RAND()) * (RAND() * RAND() / RAND()) + RAND()))"

_QUERY_SESSION_VARIABLE = "@value"
_TEMP_TABLE = "_temp_table"

def create_temp_table(primary_key_type="INT (11)"):
    return f"""
        DROP TABLE IF EXISTS {_TEMP_TABLE};
        CREATE TABLE {_TEMP_TABLE}
        (
            {_TEMP_TABLE}_id BIGINT (20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            PK {primary_key_type} DEFAULT NULL,
            anonymized_PK {primary_key_type} DEFAULT NULL
        );
        CREATE INDEX PK_INDEX ON {_TEMP_TABLE} (PK)"""


def _get_sql_type(data_type):
    return _FAKE_COLUMN_TYPES[data_type]


def _get_column_subquery(seed_table_name, column_strategy):
    if column_strategy.strategy_type == UpdateColumnStrategyTypes.EMPTY:
        return "('')"
    elif column_strategy.strategy_type == UpdateColumnStrategyTypes.UNIQUE_EMAIL:
        return f"( SELECT CONCAT({_RAND_MD5}, '@', {_RAND_MD5}, '.com') )"
    elif column_strategy.strategy_type == UpdateColumnStrategyTypes.UNIQUE_LOGIN:
        return f"( SELECT {_RAND_MD5} )"
    elif column_strategy.strategy_type == UpdateColumnStrategyTypes.FAKE_UPDATE:
        return f"( {_QUERY_SESSION_VARIABLE} := FLOOR(10 + (RAND() * 900000000 * RAND())))" if (column_strategy.qualifier == "random_int") else f"( SELECT {_QUERY_SESSION_VARIABLE} := `{column_strategy.qualifier}` FROM `{seed_table_name}` ORDER BY FLOOR(10 + (RAND() * 900000000 * RAND())) LIMIT 1)"
    elif column_strategy.strategy_type == UpdateColumnStrategyTypes.GIANT_UPDATE:
        return f"( {_QUERY_SESSION_VARIABLE} := FLOOR(10 + (RAND() * 9000000000000 * RAND())))"
    elif column_strategy.strategy_type == UpdateColumnStrategyTypes.LITERAL:
        return column_strategy.value
    else:
        raise UnsupportedColumnStrategyError(column_strategy)

def _drop_column_subquery(table_name, column_name):
    return f"ALTER TABLE {table_name} DROP COLUMN {column_name}"

def _escape_sql_value(value):
    """
    return a sql-ified version of a seed column's value
    Normally this defines the stringification of datatypes and escaping for strings
    """
    if isinstance(value, (str, datetime, date)):
        return "'" + str(value).replace("'", "''") + "'"
    else:
        return str(value)


def get_truncate_table(table_name):
    return f"SET FOREIGN_KEY_CHECKS=0; TRUNCATE TABLE `{table_name}`; SET FOREIGN_KEY_CHECKS=1;"


def get_create_seed_table(table_name, qualifier_map):
    if len(qualifier_map) < 1:
        raise ValueError("Cannot create a seed table with no columns")

    create_columns = [
        f"`{qualifier}` {_get_sql_type(strategy.data_type)}" for qualifier, strategy in qualifier_map.items()]

    return "CREATE TABLE `{}` ({});".format(table_name, ",".join(create_columns))


def get_drop_seed_table(table_name):
    return f"DROP TABLE IF EXISTS `{table_name}`;"


def get_insert_seed_row(table_name, qualifier_map):

    column_names = ",".join(
        [f"`{qualifier}`" for qualifier in qualifier_map.keys()])
    column_values = ",".join(
        [f"{_escape_sql_value(strategy.value)}" for strategy in qualifier_map.values()])

    return "INSERT INTO `{}`({}) VALUES ({});".format(table_name, column_names, column_values)


def get_create_database(database_name):
    return f"DROP DATABASE IF EXISTS `{database_name}`; CREATE DATABASE `{database_name}`;"


def get_drop_database(database_name):
    return f"DROP DATABASE IF EXISTS `{database_name}`;"


def get_in_relation_tables(column_name):
    return f"""SELECT DISTINCT TABLE_NAME AS IN_RELATION_TABLES 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE COLUMN_NAME = {column_name}"""


def update_matching_columns(linked_tables, primary_key):
    """ UPDATE users, child 
        SET users.userID = (SELECT @value := FLOOR(RAND() * (30000 - 1000 + 1)) + 1000), child.userID = @value 
        WHERE users.userID = child.userID """
    pass


def override_random_int(seed_table_name, min_max_tuple=()):
    (min, max) = min_max_tuple
    return f"UPDATE {seed_table_name} SET random_int = (SELECT @value := FLOOR(RAND() * ({max} - {min} + 1)) + {min})"

# TODO: this where-grouping behaviour should probably return to the provider, rather than implementing as q-gen logic


def convert_conditional_assignment_values(value, type):
    if (type == 'string'):
        return f"{str(value)}"
    elif(type == 'number'):
        return int(value)
    elif(type == 'boolean'):
        return bool(value)

def build_conditional_assignment_values(conditional_assignment_dict = {'true_eval': { }, 'false_eval': { }}):
    true_eval_value = conditional_assignment_dict['true_eval']['value']
    false_eval_value = conditional_assignment_dict['false_eval']['value']

    for key in conditional_assignment_dict:
        if ('type' in conditional_assignment_dict[key].keys()):
            if (key == 'true_eval'):
                true_eval_value = convert_conditional_assignment_values(true_eval_value, conditional_assignment_dict[key]['type']);
            elif (key == 'false_eval'):
                false_eval_value = convert_conditional_assignment_values(false_eval_value, conditional_assignment_dict[key]['type']);
    
    return {'true_eval_value': true_eval_value, 'false_eval_value': false_eval_value}


    

def get_update_table(seed_table_name, update_table_strategy):
    # Whether current table has linked tables
    has_linked_tables = update_table_strategy.linked_tables and len(update_table_strategy.linked_tables) > 0
    # group on where_condition
    # build lists of update statements based on the where
    output_statements = []

    where_update_statements = {}

    linked_tables_update_statements = []

    drop_columns_statements = []

    for where, column_map in update_table_strategy.group_by_where().items():

        where_update_statements[where] = []

        for column_name, column_strategy in column_map.items():
            if column_strategy.strategy_type == UpdateColumnStrategyTypes.STRIP:
                drop_columns_statements.append(_drop_column_subquery(update_table_strategy.table_name, column_name))
            else:        
                where_update_statements[where].append("{} = {}".format(
                    column_name,
                _get_column_subquery(seed_table_name, column_strategy))
                )

        if (has_linked_tables):
            for index, table in enumerate(update_table_strategy.linked_tables):
                if (update_table_strategy.conditional_assignments):
                    predicate_values = build_conditional_assignment_values(update_table_strategy.predicate_values)

                    linked_tables_update_statements.append(f"UPDATE {table} SET {table}.{update_table_strategy.primary_key_alias or update_table_strategy.primary_key} = IF((SELECT @anonymized_value := anonymized_PK FROM {_TEMP_TABLE} WHERE {table}.{update_table_strategy.primary_key_alias or update_table_strategy.primary_key} = {_TEMP_TABLE}.PK), {predicate_values['true_eval_value'] or '@anonymized_value'}, '{predicate_values['false_eval_value']}');")
                else:
                    linked_tables_update_statements.append(f"UPDATE {table} SET {table}.{update_table_strategy.primary_key_alias or update_table_strategy.primary_key} = (SELECT anonymized_PK FROM {_TEMP_TABLE} WHERE {table}.{update_table_strategy.primary_key_alias or update_table_strategy.primary_key} = {_TEMP_TABLE}.PK);")

        # REQUIRED: the first key in any table must be its PRIMARY key
        current_table_primary_key_assignments = where_update_statements[where].pop(0) if len(where_update_statements[where]) > 0 else ""
        
        linked_tables_update_statements_query = "  ".join(linked_tables_update_statements)

        current_table_other_columns_assignments = ", ".join(where_update_statements[where])

        drop_columns_statements_query = "; ".join(drop_columns_statements) + ";"

        where_clause = f" WHERE {where}" if where else ""

        # TODO:
        #   1- Update linked_tables based on the _temp_table
        #   2- Update the rest of the original table's columns
        output_statements.append(
            f"""
                {drop_columns_statements_query}
                {create_temp_table(update_table_strategy.primary_key_type)};
                INSERT INTO {_TEMP_TABLE} (PK, anonymized_PK) SELECT t.{update_table_strategy.primary_key}, t.{update_table_strategy.primary_key} FROM {update_table_strategy.table_name} AS t;
                UPDATE {update_table_strategy.table_name}, {_TEMP_TABLE} SET {current_table_primary_key_assignments}, {_TEMP_TABLE}.anonymized_PK = {_QUERY_SESSION_VARIABLE} WHERE {update_table_strategy.table_name}.{update_table_strategy.primary_key} = {_TEMP_TABLE}.PK;""" +
                f"""{("UPDATE " + update_table_strategy.table_name + " SET " + current_table_other_columns_assignments + " " + where_clause + ";") if len(current_table_other_columns_assignments) > 0 else ""}""" +
                f"""{linked_tables_update_statements_query}"""

            if has_linked_tables 
            else 
                    f"""
                    {drop_columns_statements_query}
                    UPDATE `{update_table_strategy.table_name}` SET {current_table_primary_key_assignments}""" +
                    f""" {("," + current_table_other_columns_assignments) if len(current_table_other_columns_assignments) > 0 else ""}""" +
                    f"""{where_clause};""" 
                if current_table_primary_key_assignments 
                else
                    f"""
                    {drop_columns_statements_query}
                    """
            )

    return output_statements


def get_dumpsize_estimate(database_name):
    return "SELECT data_bytes " \
           "FROM (SELECT SUM(data_length) AS data_bytes FROM information_schema.tables WHERE table_schema = '{}') " \
           "AS data;".format(database_name)
